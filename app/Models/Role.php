<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Movie
 * @package App\Models
 * @version May 11, 2019, 7:16 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection moviePeople
 * @property string title
 * @property string release_year
 */
class Role extends Model
{
    public $timestamps = false;

    public $table = 'roles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];


    public function person()
    {
        return $this->belongsTo('App\Models\Person');
    }
}
