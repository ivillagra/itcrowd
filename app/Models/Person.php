<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Person
 * @package App\Models
 * @version May 11, 2019, 7:19 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection aliases
 * @property \Illuminate\Database\Eloquent\Collection moviePeople
 * @property string last_name
 * @property string first_name
 */
class Person extends Model
{

    public $timestamps = false;

    public $table = 'persons';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    //appending column for full name
    protected $appends = ['full_name'];

    //accesor method to get full name
    public function getFullNameAttribute() {
        return $this->first_name.' '.$this->last_name;
    }


    public $fillable = [
        'last_name',
        'first_name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'last_name' => 'string',
        'first_name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'last_name' => 'required',
        'first_name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function aliases()
    {
        return $this->hasMany('\App\Models\Alias');
    }

    //function to bring all movies
    public function movies()
    {
        return $this->belongsToMany('App\Models\Movie');
    }

    //relations to get movies in wich person was involved
    public function moviesAsDirector()
    {
        return $this->belongsToMany('App\Models\Movie')->wherePivot('role_id', '=', Role::where('name', 'Director')->first()->id);
    }
    public function moviesAsProducer()
    {
        return $this->belongsToMany('App\Models\Movie')->wherePivot('role_id', '=', Role::where('name', 'Actor/Actress')->first()->id);
    }
    public function moviesAsActor()
    {
        return $this->belongsToMany('App\Models\Movie')->wherePivot('role_id', '=', Role::where('name', 'Producer')->first()->id);
    }

}
