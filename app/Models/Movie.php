<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Romans\Filter\IntToRoman;

/**
 * Class Movie
 * @package App\Models
 * @version May 11, 2019, 7:16 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection moviePeople
 * @property string title
 * @property string release_year
 */
class Movie extends Model
{
    public $timestamps = false;

    public $table = 'movies';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    //accesor to return year on roman number
    public function getReleaseYearAttribute($value) {
        $filter = new IntToRoman();
        return $filter->filter($value).' ('.$value.')';
    }


    public $fillable = [
        'title',
        'release_year'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'release_year' => 'required'
    ];


    //relations to get people involved on a movie depending their role
    public function directors()
    {
        return $this->belongsToMany('App\Models\Person')->wherePivot('role_id', '=', Role::where('name', 'Director')->first()->id);
    }
    public function actors()
    {
        return $this->belongsToMany('App\Models\Person')->wherePivot('role_id', '=', Role::where('name', 'Actor/Actress')->first()->id);
    }
    public function producers()
    {
        return $this->belongsToMany('App\Models\Person')->wherePivot('role_id', '=', Role::where('name', 'Producer')->first()->id);
    }
}
