<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMovieAPIRequest;
use App\Http\Requests\API\UpdateMovieAPIRequest;
use App\Models\Movie;
use App\Repositories\MovieRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MovieController
 * @package App\Http\Controllers\API
 */

class MovieAPIController extends AppBaseController
{
    /** @var  MovieRepository */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepo)
    {
        $this->movieRepository = $movieRepo;
    }

    /**
     * Display a listing of the Movie.
     * GET|HEAD /movies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $movies = $this->movieRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        $movies = Movie::orderBy('id', 'desc')->get();

        return $this->sendResponse($movies->toArray(), 'Movies retrieved successfully');
    }

    /**
     * Store a newly created Movie in storage.
     * POST /movies
     *
     * @param CreateMovieAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMovieAPIRequest $request)
    {
        $input = $request->all();

        $movie = $this->movieRepository->create($input);

        return $this->sendResponse($movie->toArray(), 'Movie saved successfully');
    }

    /**
     * Display the specified Movie.
     * GET|HEAD /movies/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Movie $movie */
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            return $this->sendError('Movie not found');
        }

        return $this->sendResponse($movie->toArray(), 'Movie retrieved successfully');
    }

    /**
     * Update the specified Movie in storage.
     * PUT/PATCH /movies/{id}
     *
     * @param int $id
     * @param UpdateMovieAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMovieAPIRequest $request)
    {

        $input = $request->all();

        /** @var Movie $movie */
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            return $this->sendError('Movie not found');
        }

        $movie = $this->movieRepository->update($input, $id);

        return $this->sendResponse($movie->toArray(), 'Movie updated successfully');
    }

    /**
     * Remove the specified Movie from storage.
     * DELETE /movies/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Movie $movie */
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            return $this->sendError('Movie not found');
        }

        $movie->delete();

        return $this->sendResponse($id, 'Movie deleted successfully');
    }


    public function detailed() {

        $movies = Movie::with(['directors', 'producers', 'actors'])->orderBy('id', 'desc')->get();

        return $this->sendResponse($movies->toArray(), 'Movies retrieved successfully');
    }
}
