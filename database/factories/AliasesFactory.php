<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use App\Models\Alias;
use App\Models\Person;
use Faker\Generator as Faker;

$factory->define(Alias::class, function (Faker $faker) {

    $persons = Person::all()->pluck('id');

    return [
        'alias' => $faker->name,
        'person_id' => $faker->randomElement($persons)
    ];
});
