<?php

use App\Models\Alias;
use App\Models\Movie;
use App\Models\Person;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PersonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Creating 200 persons and a random of 1 to 3 aliases for each one
        factory(Person::class, 200)->create()->each(function ($person) {
            $person->aliases()->saveMany(factory(Alias::class, random_int(1,3))->make());
            //Seeding pivot table adding randon role
            $person->movies()->attach(Movie::all()->random()->id, ['role_id' => Role::all()->random()->id]);
        });
    }
}
