<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeingKeysPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movie_person', function (Blueprint $table) {

            $table->foreign('movie_id', 'fk_movie_person_movies')
                ->references('id')->on('movies')
            ->onDelete('cascade');
            $table->foreign('person_id', 'fk_movie_person_persons')
                ->references('id')->on('persons')
                ->onDelete('cascade');
            $table->foreign('role_id', 'fk_movie_person_role')
                ->references('id')->on('roles')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movie_person', function (Blueprint $table) {
            $table->dropForeign('fk_movie_person_movies');
            $table->dropForeign('fk_movie_person_persons');
            $table->dropForeign('fk_movie_person_role');
        });
    }
}
