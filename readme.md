# Specs

Deployed on Digital Ocean Server [LINK](http://68.183.174.61/)  
Laravel make Auth scaffolding  
Mysql Database  
Laravel migrations for schema building  
Laravel factories for seeding database with random data  
VueJS consuming the API for the Frontend

## Authentication

Laravel Passport for API Authentication  
Token configured to expire in 7 days  
To simplify the auth process i left an endpoint under web:auth middleware (/api/auth/test/token) to get a personal access (Bearer) token for admin@test.com, on real life case there are other secure options to get the token properly like password grant
  
User and password for GUI on mail

## Used Libraries

**laravel/passport**  
**laravel/telescope** for request analysis (/telescope)  
**wandersonwhcr/romans** library for roman numerals  
**infyomlabs/laravel-generator** for basic api endpoints, models, controllers and tests scaffolding from database  
**toastr** installed by npm for toast messages

## API Endpoints
Base URL http://68.183.174.61/  
Considered unsafe methods create/destroy/update  
Request Body must be on x-www-form-urlencoded for PUT/PATCH request (Laravel issue)  
Accept application/json

Without Auth

Get all plain movies  
GET api/movies

Get movies and staff  
GET api/detailed/movies  

Get a single Movie  
GET api/movies/{id}

Get all plain persons  
GET api/persons  

Get persons and movies they worked on   
GET api/detailed/persons

Get a single Person  
GET api/persons/{id}

With Auth (Bearer Token) (Test token on api/auth/test/token with web:auth middleware)  
Create a movie  
POST api/movies  
Accepted params: { title: string, release_year: year }

Create a person  
POST api/persons 
Accepted params { first_name: string, last_name: string }

Update a movie  
PUT/PATCH api/movies/{id}  
Accepted params: { title: string }

Update a person  
PUT/PATCH api/persons/{id}  
Accepted params { first_name: string, last_name: string }

Delete a movie  
DELETE api/movies/{id}

Delete a person  
DELETE api/persons/{id}

Status code 200: OK  
Status code 401: Unauthorized (Bearer Token needed)  
Status code 422: Validation issue (missing fields or different types)