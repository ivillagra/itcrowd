<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
})->middleware('auth');

//Route to get to persons admin view
Route::get('/persons', function () {
    return view('persons');
});
//Route to get to movies admin view
Route::get('/movies', function () {
    return view('movies');
});

//unprotected route to get test token for protected endpoints
Route::get('api/auth/test/token', function () {
    $adminUser = User::where('email', 'admin@test.com')->first();

    return $adminUser->createToken('Test')->accessToken;
})->middleware('auth');

//Login routes
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//Home page
Route::get('/home', 'HomeController@index')->name('home');
