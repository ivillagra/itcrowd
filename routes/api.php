<?php

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route group for protected endpoints
Route::middleware('auth:api')->group(function () {

    Route::resource('movies', 'MovieAPIController')
        ->except(['show', 'index']);
    Route::resource('persons', 'PersonAPIController')
        ->except(['show', 'index']);

    Route::get('user', function () {
        return Auth::user();
    });

});


Route::resource('movies', 'MovieAPIController')
    ->only(['show', 'index']);

//Route to get movies with associated persons
Route::get('detailed/movies', 'MovieAPIController@detailed');

Route::resource('persons', 'PersonAPIController')
    ->only(['show', 'index']);

//Route to get persons with associated movies
Route::get('detailed/persons', 'PersonAPIController@detailed');
